/*
 * Copyright (C) 2009-2018, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *                          Günther Wagner <info@gunibert.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#pragma once

#include "libgrss.h"

G_BEGIN_DECLS

#define GRSS_TYPE_FEEDS_POOL (grss_feeds_pool_get_type())
G_DECLARE_DERIVABLE_TYPE (GrssFeedsPool, grss_feeds_pool, GRSS, FEEDS_POOL, GObject)

struct _GrssFeedsPoolClass {
  GObjectClass parent;

  void (*feed_fetching) (GrssFeedsPool *pool, GrssFeedChannel *feed);
  void (*feed_ready) (GrssFeedsPool *pool, GrssFeedChannel *feed, GList *items);
};

GrssFeedsPool*  grss_feeds_pool_new ();

void            grss_feeds_pool_listen            (GrssFeedsPool *pool,
                                                   GList         *feeds);
GList*          grss_feeds_pool_get_listened      (GrssFeedsPool *pool);
int             grss_feeds_pool_get_listened_num  (GrssFeedsPool *pool);
void            grss_feeds_pool_switch            (GrssFeedsPool *pool,
                                                   gboolean       run);
SoupSession*    grss_feeds_pool_get_session       (GrssFeedsPool *pool);

G_END_DECLS
