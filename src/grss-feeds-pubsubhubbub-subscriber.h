/*
 * Copyright (C) 2011-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __FEEDS_PUBSUBHUBBUB_SUBSCRIBER_H__
#define __FEEDS_PUBSUBHUBBUB_SUBSCRIBER_H__

#include "libgrss.h"

#define GRSS_FEEDS_PUBSUBHUBBUB_SUBSCRIBER_TYPE (grss_feeds_pubsubhubbub_subscriber_get_type())
G_DECLARE_FINAL_TYPE (GrssFeedsPubsubhubbubSubscriber, grss_feeds_pubsubhubbub_subscriber, GRSS, FEEDS_PUBSUBHUBBUB_SUBSCRIBER, GObject)

struct _GrssFeedsPubsubhubbubSubscriber {
  GObject parent;
};

struct _GrssFeedsPubsubhubbubSubscriberClass {
  GObjectClass parent;
};

GrssFeedsPubsubhubbubSubscriber*  grss_feeds_pubsubhubbub_subscriber_new    ();

#endif /* __FEEDS_PUBSUBHUBBUB_SUBSCRIBER_H__ */
