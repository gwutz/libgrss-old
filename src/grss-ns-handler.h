/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __NS_HANDLER_H__
#define __NS_HANDLER_H__

#include "libgrss.h"

#define GRSS_NS_HANDLER_TYPE (grss_ns_handler_get_type ())
G_DECLARE_FINAL_TYPE (GrssNSHandler, grss_ns_handler, GRSS, NS_HANDLER, GObject)

struct _GrssNSHandler {
  GObject parent;
};

struct _GrssNSHandlerClass {
  GObjectClass parent;
};

GrssNSHandler*  grss_ns_handler_new    ();

gboolean  grss_ns_handler_channel  (GrssNSHandler *handler, GrssFeedChannel *feed, xmlNodePtr cur);
gboolean  grss_ns_handler_item    (GrssNSHandler *handler, GrssFeedItem *item, xmlNodePtr cur);

#endif /* __NS_HANDLER_H__ */
