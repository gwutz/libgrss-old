/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __FEED_HANDLER_H__
#define __FEED_HANDLER_H__

#include "libgrss.h"
#include "grss-ns-handler.h"

#define GRSS_FEED_HANDLER_TYPE (grss_feed_handler_get_type ())
G_DECLARE_INTERFACE (GrssFeedHandler, grss_feed_handler, GRSS, FEED_HANDLER, GObject)

struct _GrssFeedHandlerInterface {
  GTypeInterface parent_iface;

  void (*set_ns_handler) (GrssFeedHandler *self, GrssNSHandler *handler);
  gboolean (*check_format) (GrssFeedHandler *self, xmlDocPtr doc, xmlNodePtr cur);
  GList* (*parse) (GrssFeedHandler *self, GrssFeedChannel *feed, xmlDocPtr doc, gboolean do_items, GError **error);
};

void      grss_feed_handler_set_ns_handler  (GrssFeedHandler *self, GrssNSHandler *handler);
gboolean  grss_feed_handler_check_format    (GrssFeedHandler *self, xmlDocPtr doc, xmlNodePtr cur);
GList*    grss_feed_handler_parse           (GrssFeedHandler *self, GrssFeedChannel *feed, xmlDocPtr doc, gboolean do_items, GError **error);

#endif /* __FEED_HANDLER_H__ */
