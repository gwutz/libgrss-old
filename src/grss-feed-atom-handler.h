/*
 * Copyright (C) 2009-2015, Roberto Guido <rguido@src.gnome.org>
 *                          Michele Tameni <michele@amdplanet.it>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef __FEED_ATOM_HANDLER_H__
#define __FEED_ATOM_HANDLER_H__

#include "libgrss.h"

#define GRSS_FEED_ATOM_HANDLER_TYPE (grss_feed_atom_handler_get_type ())
G_DECLARE_FINAL_TYPE (GrssFeedAtomHandler, grss_feed_atom_handler, GRSS, FEED_ATOM_HANDLER, GObject)

struct _GrssFeedAtomHandler {
  GObject parent;
};

struct _GrssFeedAtomHandlerClass {
  GObjectClass parent;
};

GrssFeedAtomHandler*  grss_feed_atom_handler_new    ();

#endif /* __FEED_ATOM_HANDLER_H__ */
